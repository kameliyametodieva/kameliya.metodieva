import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class TelerikForumTest {

    public static final String TelerikAcademy = "https://stage-forum.telerikacademy.com/";
    public static final String email = "kmm848818@abv.bg";
    public static final String password = "Barcelona84";
    public static WebDriver driver;


    @BeforeClass
    public static void classInitializer() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\kami\\Desktop\\ChromeDriver\\chromedriver.exe");
    }


    @Before
    public void loginForumPage() {

        driver = new ChromeDriver();
        driver.get(TelerikAcademy);
        Assert.assertEquals(driver.getCurrentUrl(), TelerikAcademy);
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//span[@class='d-button-label']")).click();
        driver.findElement(By.xpath("//input[@id='Email']")).sendKeys(email);
        driver.findElement(By.id("Password")).sendKeys(password);
        driver.findElement(By.id("next")).click();
        driver.switchTo().defaultContent();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @Test
    public void createNewTopic() {

        WebElement createNewTopic = driver.findElement(By.xpath("//button[@id='create-topic']"));
        createNewTopic.click();
        WebElement newTitle = driver.findElement(By.xpath("//input[@id='reply-title']"));
        newTitle.click();
        newTitle.sendKeys("New Selenium WebDriver Test");
        WebElement createTopic = driver.findElement(By.tagName("textarea"));
        createTopic.click();
        createTopic.sendKeys( " Hello from WebDriver ");
        WebElement newTopic = driver.findElement(By.xpath("//span[contains(text(),'Create Topic')]"));
        newTopic.click();

    }

    @Test
    public void editTopic() {
        driver.findElement(By.xpath("//a[contains(text(),'New Selenium WebDriver Test')]")).click();
        driver.findElement(By.xpath("//button[@class='widget-button btn-flat edit no-text btn-icon']//*[local-name()='svg']")).click();
        WebElement editTopic = driver.findElement(By.tagName("textarea"));
        editTopic.click();
        editTopic.sendKeys(":)");
        driver.findElement(By.xpath("//span[contains(text(),'Save Edit')]")).click();

    }

    @Test
    public void deleteTopic() {
        driver.findElement(By.xpath("//a[contains(text(),'New Selenium WebDriver Test')]")).click();
        driver.findElement(By.xpath("//button[@class='widget-button btn-flat show-more-actions no-text btn-icon']//*[local-name()='svg']")).click();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        WebElement deleted = driver.findElement(By.xpath("//button[@class='widget-button btn-flat delete no-text btn-icon']"));
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        deleted.click();


    }
    @Test
    public void sendNewMessage(){
        driver.findElement(By.xpath("//a[@id='toggle-messages-menu']//*[local-name()='svg']")).click();
        driver.findElement(By.xpath("//span[contains(text(),'New Message')]")).click();
        WebElement addUser = driver.findElement(By.xpath("//input[@id='private-message-users']"));
        addUser.click();
        addUser.sendKeys("@kameliyametodieva");
        WebElement selector = driver.findElement(By.xpath("//a[@class='selected']"));
        selector.click();
        WebElement typeText = driver.findElement(By.xpath("/html[1]/body[1]/section[1]/div[1]/div[3]/div[1]/div[3]/div[1]/div[2]/textarea[1]"));
        typeText.click();
        typeText.sendKeys("Hello");
        WebElement pushEnter = driver.findElement(By.xpath("/html[1]/body[1]/section[1]/div[1]/div[3]/div[1]/div[3]/div[1]/div[2]/textarea[1]"));
        pushEnter.sendKeys(Keys.ENTER);
    }

    @After
    public void closeTab() {
        driver.close();
    }
}
