package testCases;

import com.telerikacademy.testframework.Utils;
import org.junit.Test;
import pages.GooglePage;

public class SearchInGoogleTest extends BaseTest {
	String searchCriterion= "Telerik Academy";
	@Test
	public void simpleGoogleSearch() {

		actions.changeToFrame();
		actions.clickElement("search.AgreeButton");
		Utils.getWebDriver().switchTo().defaultContent();
		actions.typeValueInField(searchCriterion, "search.Input");
		actions.waitForElementVisible("search.Button",10);
		actions.clickElement("search.Button");
		actions.waitForElementVisible("search.Result",10);
		actions.clickElement("search.Result");


		navigateToQACourseViaCard();

		actions.assertNavigatedUrl("academy.QASignUpUrl");
	}

	private void navigateToQACourseViaCard(){
		actions.clickElement("academy.AlphaAnchor");
		actions.clickElement("academy.QaGetReadyLink");
		actions.clickElement("academy.SignUpNavButton");
	}
}



