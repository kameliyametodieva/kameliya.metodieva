package pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GooglePage extends BasePage {
    final String PAGE_URL = Utils.getConfigPropertyByKey("base.url");

    public GooglePage(WebDriver driver) {
        super(driver);
        super.setUrl(PAGE_URL);
    }

    public void SearchAndOpenFirstResult(String searchTerm) {
        actions.changeToFrame();
        actions.clickElement("search.AgreeButton");
        Utils.getWebDriver().switchTo().defaultContent();
        actions.typeValueInField(searchTerm, "search.Input");
        actions.waitForElementVisible("search.Button", 10);
        actions.clickElement("search.Button");
        actions.waitForElementVisible("search.Result", 10);
        actions.clickElement("search.Result");
    }


}
