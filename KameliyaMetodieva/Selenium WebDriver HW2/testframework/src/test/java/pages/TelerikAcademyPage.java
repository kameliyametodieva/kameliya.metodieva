package pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class TelerikAcademyPage extends BasePage {
    final String PAGE_URL = Utils.getConfigPropertyByKey("academy.url");

    public TelerikAcademyPage(WebDriver driver) {
        super(driver);
        super.setUrl(PAGE_URL);
    }


    public void NavigateToQACourseViaCard(){
        actions.clickElement("academy.AlphaAnchor");
        actions.clickElement("academy.ConsentButton");
        actions.clickElement("academy.QaGetReadyLink");
        actions.clickElement("academy.SignUpNavButton");
    }

    public void AssertQAAcademySignupPageNavigated(){
        actions.assertNavigatedUrl("academy.QASignUpUrl");
    }
}